﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace cviko
{
    class Program
    {

        private static int vlaken = 100000;
        private static Thread[] t = new Thread[vlaken];
        static void Main(string[] args)
        {
            Thread.CurrentThread.Name = "hlavní";

            
            for (int i = 0; i < vlaken; i++ )
            {
                t[i] = new Thread(alg);
                t[i].Name = "" + (i+1);
                t[i].Start();
            }
            
            alg();
            Console.ReadKey();
        }

        static void alg()
        {
            //Console.WriteLine("vlakno: " + Thread.CurrentThread.Name);

            TcpClient tcpcli = new TcpClient();
            IPAddress ipAddress = IPAddress.Parse("192.168.152.129");
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, 80);
            try
            {
                tcpcli.Connect(ipEndPoint);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
